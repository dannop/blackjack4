#include "baralho.h"
#include "jogador.h"

int main(){
    //Verificando o tamanho do baralho desejado
    int tamanho;
    scanf("%d", &tamanho); 
    
    //Alocando a criando o baralho do jogo baseado no tamanho anterior
    Baralho deck = iniciaBaralho(tamanho);
    leituraBaralho(&deck);
    
    //Declarando variaveis do nome e aposta do jogador
    char *nome;
    int aposta;

    //Alocando a memoria e pedindo o nome do jogador
    nome = (char *)malloc(10*sizeof(char));
    printf("Digite seu nome:\n");
    scanf("%s", nome); 
    //Declarando o jogador e salvando o nome pedido
    Jogador player;
    player = initJogador(nome);
    free(nome);
    
    //Declarando o dealer
    Jogador dealer = initJogador("Dealer");

    char continuar_rodada;
    char continuar_jogo;

    do {
        printf("Digite sua aposta:\n");
        scanf("%d", &aposta);
        initPartida(&player, aposta);
        imprimeJogador(player);
        do {
            if (continuar_rodada == 'R') {
                int valor_roubo = 0;
                printf("Digite o valor:\n");
                scanf("%d", &valor_roubo);
                roubaCarta(&deck, valor_roubo);
            } 
            
            somaPonto(&player, sorteiaCarta(&deck));

            if (retornaPonto(player) < 21) {
                printf("Deseja continuar (S/N):\n");
                scanf(" %c", &continuar_rodada);  
            }
            else {
                continuar_rodada = 'N';
            }  
        }while(retornaPonto(player) < 21 && (continuar_rodada == 'S' || continuar_rodada == 'R'));
        
        imprimeJogador(dealer);
        if (retornaPonto(player) > 21) derrota(&player);
        else {
            if (retornaPonto(player) == 21) vitoria(&player);
            else {
                initPartida(&dealer, 0);
                do {
                    somaPonto(&dealer, sorteiaCarta(&deck));
                }while(retornaPonto(dealer) < 17);
                
                if (retornaPonto(dealer) > 21) vitoria(&player);
                else {
                    if (retornaPonto(player) >= retornaPonto(dealer)) vitoria(&player);
                    else derrota(&player);
                } 
            }
        }

        printf("Deseja fazer nova aposta (S/N):\n");
        scanf(" %c", &continuar_jogo);  
    }while(continuar_jogo == 'S' || continuar_jogo == 's');
    
    imprimeResultado(player);
    
    free(player.nome);
    free(dealer.nome);

    return 0;
}
