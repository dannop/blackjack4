#include "lista.h"

Lista criaLista(){
    Lista l = NULL;
    return l;
}

int listaVazia(Lista l){
    if (l == NULL) return 1;
    return 0;
}

Carta* criaCarta(int valor, char naipe){
    Carta *no; 
    no = (Carta *)malloc(sizeof(Carta));
    no->valor = valor;
    no->naipe = naipe;
    no->prox = NULL;

    return no;
}

void inserirNoInicio(Lista *l, Carta *nova){
    if (listaVazia(*l)) *l = nova;
    else {
        nova->prox = *l;
        (*l)->prox = nova;
    }
}

void inserirNoFim(Lista *l, Carta *nova){
    if (listaVazia(*l)){
        *l = nova;
    } 
    else {
        Lista aux = *l;
        while(aux->prox != NULL) aux = aux->prox;
        aux->prox = nova;
    }
}

void insereNaPos(Lista *l, Carta c, int pos){
    int contador = 0;
    Carta *nova = criaCarta(c.valor, c.naipe);
    
    if (listaVazia(*l)) *l = nova;
    else {
        Lista aux = *l;
        while(aux->prox != NULL && contador < pos) {
            aux = aux->prox;
            contador++;
        }
        nova->prox = aux->prox->prox;
        aux->prox = nova;
    }
}

void removeNoInicio(Lista *l){
    if (listaVazia(*l)) return;
    else {
        Lista aux = *l;
        *l = (*l)->prox;
        free(aux);
    }
}

void removeNaPos(Lista *l, int pos){
    if (listaVazia(*l)) return;
    else {
        int contador = 0;
        Lista l_aux = *l;

        //Verifico no while se existe um proximo elemento na lista e também se já chegou na posição que deseja remover
        while(l_aux->prox != NULL && contador < pos) {
            *l_aux = *l_aux->prox;
            contador++;
        }
        Lista no_aux;
        no_aux = l_aux;
        *l = (*l)->prox;
        free(no_aux); 
    }
}

void imprimirLista(Lista l){
    int contador = 0;
    //Função de teste para verificar baraulho, sendo que nao irá mostrar o ultimo elemento da lista
    while(l->prox != NULL){
        contador ++;
        printf("%d Valor:%d Naipe: %c \n", contador, l->valor, l->naipe);
        l = l->prox;
    }
}

void imprimeCarta(Lista l){
    printf("Carta: %d-%c\n", l->valor, l->naipe);
}