all: blackjack

blackjack: lista.o baralho.o jogador.o blackjack.c 
	gcc blackjack.c -o blackjack lista.o baralho.o jogador.o  -Wall

lista.o: lista.c
	gcc -c lista.c -Wall

baralho.o: baralho.c
	gcc -c baralho.c -Wall

jogador.o: jogador.c
	gcc -c jogador.c -Wall
        
clean:
	rm -rf *.o
