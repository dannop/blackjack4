#include "jogador.h"

Jogador initJogador(char *nome){
    Jogador numero_um;
    numero_um.nome = (char *)malloc(10*sizeof(char));
    strcpy(numero_um.nome, nome);
    numero_um.pont = 0;
    numero_um.total = 0;
    numero_um.aposta = 0;
    return numero_um;
}

void initPartida(Jogador *j, int aposta){
    j->aposta = aposta;
    j->pont = 0;
}

void imprimeJogador(Jogador j){
    printf("%s \n", j.nome);
}

void somaPonto(Jogador *j, int valor){
    j->pont = j->pont + valor;
}

int retornaPonto(Jogador j){
    return j.pont;
}

void imprimeResultado(Jogador j){
    if (j.total >= 0) printf("%s, voce ganhou %d reais.\n", j.nome, j.total);
    else printf("%s, voce perdeu %d reais.\n", j.nome, (-1*j.total));
}

void vitoria(Jogador *j){
    j->total = j->total + j->aposta;
    printf("Voce ganhou!\n");
}

void derrota(Jogador *j){
    j->total = j->total - j->aposta;
    printf("Voce perdeu!\n");
}
