#include "baralho.h"

Baralho iniciaBaralho(int tamanho){
    Baralho deck;
    deck.tamanho = tamanho;
    deck.posicao = 0;
    deck.cartas = criaLista();
    return deck;
}

void leituraBaralho(Baralho *deck){
    char char_original, char_naipe, lixo;
    for (int i=0; i<deck->tamanho; i++){
        scanf(" %c%c%c", &char_original, &char_naipe, &lixo );
        if(char_original=='A'){
            inserirNoFim(&deck->cartas, criaCarta(1, char_naipe));
        } 
        else {
			if(char_original=='0' || char_original=='J' || char_original=='Q' || char_original=='K'){
				inserirNoFim(&deck->cartas, criaCarta(10, char_naipe));
			}
			else {
                inserirNoFim(&deck->cartas, criaCarta(char_original-'0', char_naipe)); 
			}
		}
    }
}

int sorteiaCarta(Baralho *b){
    if (listaVazia(b->cartas)){
        printf("Nao ha mais cartas.");
        return 0;
    } 
    else {
        int valor_carta = b->cartas->valor;
        b->posicao++;
        imprimeCarta(b->cartas);
        removeNoInicio(&b->cartas);
        return valor_carta;
    }
}

void roubaCarta(Baralho *b, int valor){
    Lista aux = b->cartas;
    int pos_aux = 0;
        
    while (aux->prox != NULL && listaVazia(b->cartas)){
        if (valor != aux->valor) {
            aux = aux->prox;
            pos_aux++;
        }
        else {
            inserirNoInicio(&b->cartas, aux);
            removeNaPos(&b->cartas, pos_aux);
            b->posicao++;
        }
    }
}