#include <stdio.h>
#include <stdlib.h>

typedef struct carta{
    int valor;
    char naipe;
    struct carta* prox;
}Carta;

typedef Carta* Lista;

Lista criaLista();

int listaVazia(Lista l);

int percorreLista(Lista l);

Carta* criaCarta(int valor, char naipe);

void inserirNoInicio(Lista *l, Carta *c);

void inserirNoFim(Lista *l, Carta *c);

void insereNaPos(Lista *l, Carta c, int pos);

void removeNoInicio(Lista *l);

void removeNaPos(Lista *l, int pos);

void imprimirLista(Lista l);

void imprimeCarta(Lista l);